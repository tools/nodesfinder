let re_domain = new RegExp('[A-Za-z0-9]+\.[A-z]+'); // TODO verifier le regex
let re_ipv4 = new RegExp('/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/')
//let re_ipv4 = new RegExp('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
let re_ipv6 = new RegExp(' (([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))')
let re_path =  new RegExp('\/?[A-z0-9]+') // the [^xyz] should FORBID the elements...

let is_path = function (to_test, endpoint) {
    return (
        re_path.test(to_test)
        && Number(to_test) != endpoint.port
        && ! /[:.]/.test(to_test) // no ipv4/6
    )
}

class BMAEndpoint{

    constructor(raw_endpoint, pubkey) {
        this.pubkey = pubkey
        let infos = raw_endpoint.split(" ");

        this.type = this.get_endpoint_type(infos);
        if (! this.type) {
            return false;
        }
        if (protocol_is_https && this.type != 'BMAS') {
            return false
        }
        this.domain = this.get_endpoint_domain(infos);
        this.ipv4 = this.get_endpoint_ipv4(infos);
        //this.ipv6 = get_endpoint_ipv6(infos);
        this.port = this.get_endpoint_port(infos)
        this.path = this.get_endpoint_path(infos)

        // TODO manage IPv4/v6

        this.has_url = this.create_root_url();
        this.is_member();
    }

    get_endpoint_type(raw_endpoint_array) {
        if (raw_endpoint_array[0] == "BASIC_MERKLED_API") {
            return "BMA"
        } else if (raw_endpoint_array[0] == "BMAS") {
            return "BMAS"
        } else {
            return false
        }
    }

    get_endpoint_domain = function (raw_endpoint_array) {
        if (re_domain.test(raw_endpoint_array[1]) && ! raw_endpoint_array[1].includes('/')) {
            return raw_endpoint_array[1]
        }
    }

    get_endpoint_ipv4 = function(raw_endpoint_array) {
        let a =  1;
        do {
            if (re_ipv4.test(raw_endpoint_array[a])) {
                return raw_endpoint_array[a]
            }
            a++ 
        } while (a <= 2);
    }

    get_endpoint_ipv6 = function(raw_endpoint_array) {
        let a =  1;
        do {
            if (re_ipv6.test(raw_endpoint_array[a])) {
                return raw_endpoint_array[a]
            }
            a++ 
        } while (a <= 3);
    }

    get_endpoint_port = function(raw_endpoint_array) {
        let a =  2;
        do {
            if (! isNaN(Number(raw_endpoint_array[a]))) {
                return Number(raw_endpoint_array[a]);
            }
            a++ 
        } while (a < raw_endpoint_array.length);
    }

    get_endpoint_path = function(raw_endpoint_array) {
        let a =  3;
        do {
            if (is_path(raw_endpoint_array[a], this)) {
                return raw_endpoint_array[a]
            }
            a++ 
        } while (a < raw_endpoint_array.length);
    }

    create_root_url () {
        this.url_root = '';
        switch (this.type) {
            case 'BMA':
                this.url_protocol = "http://"
                break
            case 'BMAS':
                this.url_protocol = "https://"
                break
            default: 
                return false
        }

        if (this.domain) {
            this.url_root += this.domain
        } else if (this.ipv4) {
            this.url_root += this.ipv4
        } else {
            return false
        }

        if (this.port) {
            this.url_root += ':' + this.port
        } else {
            return false
        }

        if (this.path) {
            this.url_root += this.path
        }

        this.url_base = this.url_protocol + this.url_root
        return true;
    }

    get_head = async function() {
        let url_block = this.url_protocol + this.url_root + '/blockchain/current'
        console.log(this.url_block)
        let start = Date.now();
        
        let config = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'text/html',
                'Cache-Control': 'no-cache'
            },
        }
                
        return fetch(url_block, config)
        .then(response => {
            if (response.status == 200) {
                this.reachable = true
                this.speed = Date.now() - start;
                return response.json()
            } else {
                this.reachable = false;
            }
        })
        .then(data => {
            this.head_block = data.number
        })
        .catch(error => {
            this.reachable = false;
        })
    }

    draw_row(fields) {
        this.row = document.createElement('tr')
        fields.forEach(field => {
            let cell = document.createElement('td')
            cell.innerText = (this[field] == undefined) ? 'ø' : this[field]
            this.row.appendChild(cell)
        });
    }

    get_version = async function() {
        return fetch(this.url_protocol + this.url_root + '/node/summary/')
        .then(response => response.json())
        .then(data => this.version = data.duniter.version)
        .catch(error => { this.version = "ø" }) 
    }

    is_member = async function() {
        let url = this.url_protocol + this.url_root + '/wot/identity-of/' + this.pubkey
        return fetch(url)
        .then(response => response.json())
        .then(data => {
            this.member = (data.pubkey ? data.uid : 'Non')
        })
        .catch(error => { this.member = "ø" }) 
    }
}