class DuniterNodes
{
    constructor (peers) {
        this.fields = ['url_root', 'head_block', 'type', 'speed', 'version', 'member']
        this.forget_versions = ['ø']
        this.upgrade_versions = ['1.8.7', '1.7.23', '1.9.0']

        this.ep_body = document.createElement('tbody')
        this.draw()

        console.log ('Build nodes list...')
        this.endpoints = []
        this.extract_endpoints(peers)
        this.get_endpoints(this.fields)

        this.count_blocks = {}
        this.count_blocks.blocks = []
        
    }

    extract_endpoints = function (peers) {
        console.log(peers)
        peers.forEach(peer => {
            let endpoints = peer.endpoints;
            endpoints.forEach(endpoint => {
                let ep = new BMAEndpoint(endpoint, peer.pubkey)
                if (ep.has_url) {
                    this.endpoints.push(ep)
                }
            })
        });
    }

    get_endpoints = async function (fields) {
        let tmp_endpoints = this.endpoints
        tmp_endpoints.forEach((ep) => {
            let block = ep.get_head();
            block.then(data => {
                this.stats_blocks(ep)
                ep.draw_row(fields)
                this.insert(ep)
            })
            ep.get_version()
        })      
    }

    insert(endpoint) {
        let tmp_endpoints = this.endpoints
        let head_list = []
        let written_urls = []
        let new_tbody = document.createElement('tbody')

        if (endpoint.reachable) {
            let pos = tmp_endpoints.indexOf(endpoint);
            tmp_endpoints.splice(pos, 1)

            // if there are many pubkeys for a node and this is the member pubkey, consider the node is member.
            if (['Non', 'ø', undefined].indexOf(endpoint.member) > 0) {
                $row_to_replace = Array.from(document.getElementsByTagName('url', endpoint.root_url))[0];
                if ($row_to_replace) {
                    $cells = Array.from($row_to_replace.childNodes)
                    $cells[5].innerText = endpoint.member;
                }
            }

            // trier par bloc
            while (tmp_endpoints[0]  && tmp_endpoints[0].head_block >= endpoint.head_block) {
                let current_ep = tmp_endpoints.shift()

                if (! written_urls.includes(current_ep.url_base)) {
                    written_urls.push(current_ep.url_base)
                    new_tbody.appendChild(current_ep.row)
                    head_list.push(current_ep)
                }
            }
            
            // draw empty elements
            tmp_endpoints.forEach(ep => {
                ep.draw_row(this.fields)
                if (! written_urls.includes(ep.url_base)) {
                    written_urls.push(ep.url_base)
                    new_tbody.appendChild(ep.row)
                }
            })

            this.endpoints = head_list.concat([endpoint]).concat(tmp_endpoints)

            this.ep_body = new_tbody
            this.draw()
        }
    }

    stats_blocks(endpoint){
        // count blocks for majority
        if (endpoint.head_block != undefined) {
            if (! this.count_blocks['b_' + endpoint.head_block]) {
                this.count_blocks['b_' + endpoint.head_block] = 1
                this.count_blocks.blocks.push('b_' + endpoint.head_block)
            } else {
                this.count_blocks['b_' + endpoint.head_block] += 1
            }
        }
    }

    draw() {
        this.ep_table = document.getElementById('endpoints_table')
        let table_head = document.getElementById('table_head')

        this.ep_table.innerHTML = ''
        this.ep_table.appendChild(table_head)
        this.ep_table.appendChild(this.ep_body)

        let consensus_block = this.getConsensusBlock()
        let last_version = this.getLastVersion()
        let rows = this.ep_body.childNodes
        rows.forEach(row => {
            let cells = row.childNodes

            switch (cells[1].innerText) {
                case consensus_block:
                    cells[1].setAttribute('class', 'ok')
                    break
                case 'undefined':
                    cells[1].setAttribute('class', 'empty')
                    break
                case 'ø':
                    cells[1].setAttribute('class', 'empty')
                    break
                default:
                    cells[1].setAttribute('class', 'warn')
                    break
            }

            switch(cells[5].innerText) {
                case 'Non':
                    cells[5].setAttribute('class', 'warn')
                    break
                case 'undefined':
                    cells[4].setAttribute('class', 'empty')
                    break
                case 'ø':
                    cells[4].setAttribute('class', 'empty')
                    break
                default:
                    cells[5].setAttribute('class', 'ok')
                    break

            }

            switch(cells[4].innerText) {
                case last_version:
                    cells[4].setAttribute('class', 'ok')
                    break
                case 'undefined':
                    cells[4].setAttribute('class', 'empty')
                    break
                case 'ø':
                    cells[4].setAttribute('class', 'empty')
                    break
                default:
                    cells[4].setAttribute('class', 'warn')
                    break
            }

            // added to follow 1.8.3->1.8.5 fork
            if (this.upgrade_versions.find(ver => ver == cells[4].innerText)) {
                cells[4].setAttribute('class', 'ok')
            }
        })
    }

    getConsensusBlock() {
        if (this.count_blocks == undefined) {
            return 0
        }

        let max = 0
        let consensus_block = ''
        this.count_blocks.blocks.forEach((block) => {
            if (this.count_blocks[block] > max) {
                max = this.count_blocks[block]
                consensus_block = block.replace('b_', '')
            }
        })

        return consensus_block
    }

    getLastVersion() {
        if (this.count_blocks == undefined) {
            return 0
        }

        let max_version = ''
        this.endpoints.forEach((ep) => {
            if (ep.version > max_version && ! this.forget_versions.find( elt => elt == ep.version)) {
                max_version = ep.version
            }
        })

        console.log('max version', max_version)
        return String(max_version)
    }
}