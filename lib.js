/*
function duniter_fetch(url) {
    console.log('duniter_fetch', url)
    let return_json = ''

    return new Promise((resolve, reject) => {
        console.log('promise')
        let temp = document.createElement('script')
        temp.src = url
        temp.addEventListener('load', (event) => {
            console.log('onload', url)
            console.log(temp.innerText)
            if (temp.innerText != '') {
                return_json = temp.innerText
                console.log(return_json)
                resolve(return_json)
            }
            reject('could not get data from ' + url)
        })
        document.body.appendChild(temp)
    }) 
}
*/

function UniqueEndpoint(value, index, self) {
    return self.indexOf(value) === index;
}